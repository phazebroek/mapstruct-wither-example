package nl.phazebroek;

public class Bar {

  private String someOtherProperty;

  public String getSomeOtherProperty() {
    return someOtherProperty;
  }

  public void setSomeOtherProperty(String someOtherProperty) {
    this.someOtherProperty = someOtherProperty;
  }

  public Bar withSomeOtherProperty(String someOtherProperty) {
    this.someOtherProperty = someOtherProperty;
    return this;
  }
}
