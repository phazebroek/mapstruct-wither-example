package nl.phazebroek;

public class Foo {

  private String someProperty;

  public String getSomeProperty() {
    return someProperty;
  }

  public void setSomeProperty(String someProperty) {
    this.someProperty = someProperty;
  }

  public Foo withSomeProperty(String someProperty) {
    this.someProperty = someProperty;
    return this;
  }
}
