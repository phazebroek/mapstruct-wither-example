package nl.phazebroek;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface FooMapper {

  @Mapping(target = "someOtherProperty", source = "someProperty")
  Bar toBar(Foo source);

}
