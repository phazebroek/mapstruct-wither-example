package nl.phazebroek;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class FooMapperTest {

  FooMapper mapper = new FooMapperImpl();

  @Test
  void testMapping() {
    Foo foo = new Foo();
    foo.setSomeProperty("a");

    Bar bar = mapper.toBar(foo);

    assertThat(bar.getSomeOtherProperty(), is("a"));
  }
}
